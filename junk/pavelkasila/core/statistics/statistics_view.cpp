//
// Created by Pavel Kasila on 7.05.24.
//

#include "statistics_view.h"

#include "statistics_chart_view.h"
#include "statistics_export_view.h"
#include "statistics_mode.h"

#include <QComboBox>
#include <QVBoxLayout>
#include <memory>

namespace outfit::core::statistics {
StatisticsView::StatisticsView() {
    layout_manager_ = std::make_unique<QVBoxLayout>(this);
    chart_view_ = std::make_unique<StatisticsChartView>(mode_);
    export_view_ = std::make_unique<StatisticsExportView>(mode_);
    mode_selector_ = std::make_unique<QComboBox>();

    layout_manager_->addWidget(mode_selector_.get());
    layout_manager_->addWidget(chart_view_.get());
    layout_manager_->addWidget(export_view_.get());

    mode_selector_->addItems({
      "Categories",
      "Material",
      "Sizes",
      "Seasons",
    });
    mode_selector_->setCurrentIndex(0);

    connect(mode_selector_.get(), &QComboBox::currentIndexChanged, this, &StatisticsView::setMode);
}

void StatisticsView::setMode(int index) {
    mode_ = static_cast<StatisticsMode>(index);
    chart_view_->setMode(mode_);
    export_view_->setMode(mode_);
    chart_view_->update();
}
}  // namespace outfit::core::statistics
