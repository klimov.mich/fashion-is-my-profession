//
// Created by Pavel Kasila on 7.05.24.
//

#include "statistics_chart_view.h"

#include "chart_callout.h"
#include "statistics_mode.h"

#include <QCategoryAxis>
#include <QLineSeries>
#include <QMessageBox>
#include <QPainter>
#include <QPolarChart>
#include <QSizePolicy>
#include <QSqlQuery>
#include <QTimer>
#include <QValueAxis>
#include <algorithm>
#include <cmath>
#include <memory>

namespace outfit::core::statistics {
StatisticsChartView::StatisticsChartView(StatisticsMode mode) : mode_(mode) {
    chart_ = std::make_unique<QPolarChart>();
    setContentsMargins(0, 0, 0, 0);

    angular_axis_ = std::make_unique<QCategoryAxis>();
    angular_axis_->setLabelsPosition(QCategoryAxis::AxisLabelsPositionOnValue);

    quantity_series_ = std::make_unique<QLineSeries>();
    quantity_series_->setName("Quantity");

    chart_->addSeries(quantity_series_.get());
    chart_->addAxis(angular_axis_.get(), QPolarChart::PolarOrientationAngular);

    radial_axis_ = std::make_unique<QValueAxis>();
    radial_axis_->setTickCount(10);
    radial_axis_->setLabelFormat("%d");
    chart_->addAxis(radial_axis_.get(), QPolarChart::PolarOrientationRadial);

    quantity_series_->attachAxis(radial_axis_.get());
    quantity_series_->attachAxis(angular_axis_.get());

    radial_axis_->setRange(0, 100);

    setChart(chart_.get());
    setRenderHint(QPainter::Antialiasing);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    update();

    timer_ = std::make_unique<QTimer>(this);
    connect(timer_.get(), &QTimer::timeout, this, &StatisticsChartView::update);
    timer_->start(1000);

    connect(quantity_series_.get(), &QLineSeries::hovered, this, &StatisticsChartView::tooltip);
}

StatisticsChartView::~StatisticsChartView() {
    timer_->stop();
}

void StatisticsChartView::update() {
    QSqlQuery query;

    if (!query.exec(categoryCountQuery()) || !query.next()) {
        QMessageBox msg;
        msg.setText("Failed to fetch categories count.");
        msg.exec();
        return;
    }

    const int size = query.value(0).toInt();
    angular_axis_->setRange(0, size);

    if (!query.exec(countDataQuery())) {
        QMessageBox msg;
        msg.setText("Failed to fetch categories.");
        msg.exec();
        return;
    }

    int i = 0;
    int max_value = 1;

    for (const QString& label : angular_axis_->categoriesLabels()) {
        angular_axis_->remove(label);
    }

    quantity_series_->clear();

    bool first_exists = false;
    int first_value = 0;

    while (query.next()) {
        i++;
        max_value = std::max(query.value(2).toInt(), max_value);
        angular_axis_->append(query.value(1).toString(), i);
        *quantity_series_ << QPointF(i - 1, query.value(2).toInt());
        if (i == 1) {
            first_exists = true;
            first_value = query.value(2).toInt();
        }
    }

    if (first_exists) {
        *quantity_series_ << QPointF(size, first_value);
    }

    radial_axis_->setRange(0, max_value);
    radial_axis_->setTickCount(std::min(10, max_value + 1));
}

void StatisticsChartView::setMode(StatisticsMode mode) {
    mode_ = mode;
}

QString StatisticsChartView::categoryCountQuery() {
    switch (mode_) {
        case StatisticsMode::Sizes:
            return "select count(ic.id) from item_sizes ic";
        case StatisticsMode::Seasons:
            return "select count(ic.id) from seasons ic";
        case StatisticsMode::Materials:
            return "select count(ic.id) from item_materials ic";
        case StatisticsMode::Categories:
        default:
            return "select count(ic.id) from item_categories ic";
    }
}

QString StatisticsChartView::countDataQuery() {
    switch (mode_) {
        case StatisticsMode::Sizes:
            return "select ic.id, ic.name, (select count(id) from items i "
                   "where i.size_id = ic.id) as count from item_sizes ic";
        case StatisticsMode::Seasons:
            return "select ic.id, ic.name, (select count(id) from items i "
                   "where i.season_id = ic.id) as count from seasons ic";
        case StatisticsMode::Materials:
            return "select ic.id, ic.name, (select count(id) from items i "
                   "where i.material_id = ic.id) as count from item_materials ic";
        case StatisticsMode::Categories:
        default:
            return "select ic.id, ic.name, (select count(id) from items i "
                   "where i.category_id = ic.id) as count from item_categories ic";
    }
}

void StatisticsChartView::tooltip(QPointF point, bool state) {
    if (tooltip_ == nullptr) {
        tooltip_ = std::make_unique<ChartCallout>(chart_.get());
    }

    if (state) {
        tooltip_->setText(QString("Value: %2").arg(std::round(point.y())));
        tooltip_->setAnchor(point);
        tooltip_->setZValue(11);
        tooltip_->updateGeometry();
        tooltip_->show();
    } else {
        tooltip_->hide();
    }
}
}  // namespace outfit::core::statistics
