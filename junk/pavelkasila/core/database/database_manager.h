//
// Created by Pavel Kasila on 24.04.24.
//

#ifndef CREATIVE_DATABASE_MANAGER_H
#define CREATIVE_DATABASE_MANAGER_H

#include <QSqlDatabase>
#include <QString>
#include <memory>

namespace outfit::core::database {
class DatabaseManager {
   public:
    DatabaseManager();

    ~DatabaseManager();

    bool connect();

    static DatabaseManager& getInstance() {
        static DatabaseManager instance;  // This is THE instance
        return instance;
    }

    DatabaseManager(DatabaseManager&) = delete;
    DatabaseManager(DatabaseManager&&) = delete;
    DatabaseManager& operator=(DatabaseManager& other) = delete;
    DatabaseManager& operator=(DatabaseManager&& other) = delete;

   private:
    static bool applyMigrations();

    static bool migrationCreateTables();

    static bool migrationAddItemCategories();

    static bool migrationAddItemMaterials();

    static bool migrationAddItemSizes();

    static bool migrationAddSeasons();

    QString path_;
    std::unique_ptr<QSqlDatabase> database_;
};
}  // namespace outfit::core::database

#endif  // CREATIVE_DATABASE_MANAGER_H
