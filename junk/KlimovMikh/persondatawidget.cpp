#include "persondatawidget.h"
#include <QIcon>
#include <QGroupBox>
#include <QHeaderView>

PersonDataWidget::PersonDataWidget(QWidget *parent) : QWidget(parent) {
    setupUI();
}

void PersonDataWidget::setupUI() {
    this->setFixedSize(400, 400);
    tableWidget = new QTableWidget(this);
    tableWidget->setColumnCount(2);
    tableWidget->horizontalHeader()->setStretchLastSection(true);
    tableWidget->verticalHeader()->setVisible(false);
    tableWidget->setRowCount(22);
    tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QStringList headers = {"Field", "Value"};
    tableWidget->setHorizontalHeaderLabels(headers);

        QVBoxLayout *layout = new QVBoxLayout(this);
        layout->addWidget(tableWidget);
        setLayout(layout);

        setStyleSheet(R"(
            QTableWidget {
                border: 1px solid #c0c0c0;
                border-radius: 5px;
                font-size: 14px;
            }
            QTableWidget::item {
                padding: 10px;
            }
            QHeaderView::section {
                background-color: #f0f0f0;
                padding: 5px;
                border: 1px solid #d0d0d0;
                font-size: 14px;
            }
        )");
}

void PersonDataWidget::setPersonData(const PersonData &data) {
    tableWidget->setItem(0, 0, new QTableWidgetItem("Name"));
    tableWidget->setItem(0, 1, new QTableWidgetItem(data.name));

    tableWidget->setItem(1, 0, new QTableWidgetItem("Age (years)"));
    tableWidget->setItem(1, 1, new QTableWidgetItem(QString::number(data.generalData.age)));

    tableWidget->setItem(2, 0, new QTableWidgetItem("Weight type"));
    QString weightString;
    switch (data.bodyData.weight_type) {
        case WeightType::OBESE: weightString = "Obese"; break;
        case WeightType::DEFAULT: weightString = "Undefined"; break;
        case WeightType::NORMAL: weightString = "Normal"; break;
        case WeightType::UNDER_WEIGHT: weightString = "Underweight"; break;
        case WeightType::OVER_WEIGHT: weightString = "Overweight"; break;
    }
    tableWidget->setItem(2, 1, new QTableWidgetItem(weightString));

    tableWidget->setItem(3, 0, new QTableWidgetItem("Hair Color"));
    QString hairString;
    switch (data.generalData.hair_color) {
        case HairColor::light_hair: hairString = "Light"; break;
        case HairColor::dark_hair: hairString = "Dark"; break;
        case HairColor::default_hair: hairString = "Undefined"; break;
    }
    tableWidget->setItem(3, 1, new QTableWidgetItem(hairString));

    tableWidget->setItem(4, 0, new QTableWidgetItem("Skin Color"));
    QString skinString;
    switch (data.generalData.skin_color) {
        case SkinColor::light_skin: skinString = "Light"; break;
        case SkinColor::dark_skin: skinString = "Dark"; break;
        case SkinColor::default_skin: skinString = "Undefined"; break;
    }
    tableWidget->setItem(4, 1, new QTableWidgetItem(skinString));

    tableWidget->setItem(5, 0, new QTableWidgetItem("Eyes Color"));
    QString eyesString;
    switch (data.generalData.eye_color) {
        case EyeColor::brown: eyesString = "Brown"; break;
        case EyeColor::green: eyesString = "Green"; break;
        case EyeColor::default_eyes: eyesString = "Undefined"; break;
        case EyeColor::blue: eyesString = "Blue"; break;
    }
    tableWidget->setItem(5, 1, new QTableWidgetItem(eyesString));

    tableWidget->setItem(6, 0, new QTableWidgetItem("Face shape"));
    tableWidget->setItem(6, 1, new QTableWidgetItem(data.generalData.face_shape));

    tableWidget->setItem(7, 0, new QTableWidgetItem("Eyes shape"));
    tableWidget->setItem(7, 1, new QTableWidgetItem(data.generalData.eye_shape));

    tableWidget->setItem(7, 0, new QTableWidgetItem("Face features"));
    tableWidget->setItem(7, 1, new QTableWidgetItem(data.generalData.face_features));

    tableWidget->setItem(8, 0, new QTableWidgetItem("Body features"));
    tableWidget->setItem(8, 1, new QTableWidgetItem(data.generalData.body_features));

    tableWidget->setItem(9, 0, new QTableWidgetItem("Social status"));
    tableWidget->setItem(9, 1, new QTableWidgetItem(data.generalData.social_status));

    tableWidget->setItem(10, 0, new QTableWidgetItem("Height (cm)"));
    tableWidget->setItem(10, 1, new QTableWidgetItem(QString::number(data.bodyData.height)));

    tableWidget->setItem(11, 0, new QTableWidgetItem("Weight (kg)"));
    tableWidget->setItem(11, 1, new QTableWidgetItem(QString::number(data.bodyData.weight)));

    tableWidget->setItem(12, 0, new QTableWidgetItem("Chest girth (kg)"));
    tableWidget->setItem(12, 1, new QTableWidgetItem(QString::number(data.bodyData.chest_girth)));

    tableWidget->setItem(13, 0, new QTableWidgetItem("Waist girth (kg)"));
    tableWidget->setItem(13, 1, new QTableWidgetItem(QString::number(data.bodyData.waist_girth)));

    tableWidget->setItem(14, 0, new QTableWidgetItem("Hip girth (kg)"));
    tableWidget->setItem(14, 1, new QTableWidgetItem(QString::number(data.bodyData.hip_girth)));

    tableWidget->setItem(15, 0, new QTableWidgetItem("Body fat (kg)"));
    tableWidget->setItem(15, 1, new QTableWidgetItem(QString::number(data.bodyData.body_fat)));

    tableWidget->setItem(16, 0, new QTableWidgetItem("Muscle mass (kg)"));
    tableWidget->setItem(16, 1, new QTableWidgetItem(QString::number(data.bodyData.muscle)));

    tableWidget->setItem(17, 0, new QTableWidgetItem("Proteins (kg)"));
    tableWidget->setItem(17, 1, new QTableWidgetItem(QString::number(data.bodyData.protein)));

    tableWidget->setItem(18, 0, new QTableWidgetItem("Visceral fat (kg)"));
    tableWidget->setItem(18, 1, new QTableWidgetItem(QString::number(data.bodyData.visceral_fat)));

    tableWidget->setItem(19, 0, new QTableWidgetItem("Basal metabolism (kg)"));
    tableWidget->setItem(19, 1, new QTableWidgetItem(QString::number(data.bodyData.basal_metabolism)));

    tableWidget->setItem(20, 0, new QTableWidgetItem("Bmi (kg)"));
    tableWidget->setItem(20, 1, new QTableWidgetItem(QString::number(data.bodyData.bmi)));
}
