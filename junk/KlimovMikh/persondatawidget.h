#ifndef PERSONDATAWIDGET_H
#define PERSONDATAWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QVBoxLayout>
#include "PersonData.h"
#include <QTableWidget>

class PersonDataWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PersonDataWidget(QWidget *parent = nullptr);

    void setPersonData(const PersonData &data);

private:
    QTableWidget *tableWidget;

    void setupUI();
};

#endif // PERSONDATAWIDGET_H
