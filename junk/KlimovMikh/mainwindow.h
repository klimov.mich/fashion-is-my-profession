#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "persondatawidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

    void setWidget(const PersonData& d);

    ~MainWindow();

private:
    Ui::MainWindow *ui;
    PersonDataWidget *data;
};
#endif // MAINWINDOW_H
